#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys

from __init__ import spike_logger, except_hook
from spike_database.Contests import Contests
from spike_database.Matches import Matches
from spike_model.Match import Match
from spike_standings.Utilities import Utilities
from spike_utilities.Utilities import Utilities as SpikeUtilities


def main():
    spike_logger.info("Update start")
    update()
    spike_logger.info("Update finished")
    sys.exit()


def update():
    matches_db = Matches()
    contest_db = Contests()
    matches = matches_db.get_match_unsaved_in_contests()
    match_ids = []
    competitions = []
    excluded_competition = []

    nb_match = len(matches)
    spike_logger.info(f"Update from: {nb_match} matches")

    for match in matches:
        uuid = match.get("uuid")
        if uuid is not None:
            spike_logger.info(f"  {uuid}")
            match_ids.append(uuid)
            match = Match(match)
            match.set_uuid(uuid)
            competitions.append((match.get_competition_name(), match.get_competition_id(), match.get_platform_id()))
            contest_db.update_contest_from_match(match)
            if SpikeUtilities.is_cabal_vision(match.get_league_id(), match.get_platform_id()):
                excluded_competition.append((match.get_competition_name(), match.get_competition_id(), match.get_platform_id()))

    if len(match_ids) > 0:
        matches_db.set_match_saved_in_contests(match_ids)

    # Update ranking
    if nb_match < 500:
        competitions = set(competitions)
        utils = Utilities()
        for competition in competitions:
            if competition not in excluded_competition and competition[1] > 0:
                spike_logger.info(f"Update ranking for {competition[0]} :: {competition[1]} :: {competition[2]}")
                utils.compute_standing(competition[1], competition[2])
                spike_logger.info(f"Update impact players for {competition[0]} :: {competition[1]} :: {competition[2]}")
                utils.compute_impact_players_ranking(competition[1], competition[2])
            else:
                spike_logger.info(f"Ignore competition {competition[0]} :: {competition[1]} :: {competition[2]}")
    else:
        update()


sys.excepthook = except_hook

if __name__ == "__main__":
    main()
