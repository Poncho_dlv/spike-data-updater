#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys

from __init__ import spike_logger, except_hook
from spike_database.Bounty import Bounty
from spike_database.Coaches import Coaches
from spike_database.Matches import Matches
from spike_database.Players import Players
from spike_database.Teams import Teams
from spike_model.Match import Match
from spike_requester.Utilities import Utilities


def main():
    spike_logger.info("Update start")
    update()
    spike_logger.info("Update finished")
    sys.exit()


def update():
    matches_db = Matches()
    team_db = Teams()
    player_db = Players()
    coach_db = Coaches()

    matches = matches_db.get_match_to_check_for_update_team_and_player()
    match_ids = []

    nb_match = len(matches)
    spike_logger.info(f"Update from: {nb_match} matches")

    for match in matches:
        uuid = match.get("uuid")
        spike_logger.info(uuid)
        try:
            if uuid is not None:
                spike_logger.debug(f"  {uuid}")
                match_ids.append(uuid)
                match = Match(match)
                match.set_uuid(uuid)
                if match.get_start_datetime() != match.get_finish_datetime():
                    team_db.add_or_update_team(match.get_team_home(), match.get_platform_id())
                    for player in match.get_team_home().get_players():
                        player_db.add_or_update_player(player, match.get_platform_id(), match.get_team_home().get_id())
                    team_db.add_or_update_team(match.get_team_away(), match.get_platform_id())
                    for player in match.get_team_away().get_players():
                        player_db.add_or_update_player(player, match.get_platform_id(), match.get_team_away().get_id())
        except AttributeError:
            spike_logger.error(f"Invalid match data: {uuid}")
            try:
                Utilities.get_match(uuid, force_refresh=True)
            except:
                continue

    if len(match_ids) > 0:
        matches_db.set_match_to_check_for_update_team_and_player(match_ids)

    bounty_db = Bounty()
    bounty_list = bounty_db.get_all_bounty_data()

    for bounty in bounty_list:
        spike_logger.info(f"Bounty: {bounty.get('player_id')}")
        platform_id = bounty.get("platform_id")
        team_data = team_db.get_team(bounty.get("team_id"), platform_id)
        player_data = player_db.get_player(bounty.get("player_id"), platform_id)

        if team_data and player_data:
            issuers = bounty.get("issuers")
            fixed_issuers = []
            for issuer in issuers:
                try:
                    fixed_issuers.append(int(issuer))
                except:
                    pass
            fixed_issuers = list(set(fixed_issuers))
            coach_name = coach_db.get_coach_name(team_data.get("coach_id"), platform_id)
            if coach_name:
                bounty_db.add_or_update_bounty(player_data, team_data, platform_id, fixed_issuers, coach_name)
            else:
                spike_logger.error(f"Invalid Bounty: {bounty} unable to find coach")
        else:
            spike_logger.error(f"Invalid Bounty: {bounty}")


sys.excepthook = except_hook

if __name__ == "__main__":
    main()
