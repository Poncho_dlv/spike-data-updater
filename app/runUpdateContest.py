#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys

from __init__ import spike_logger, except_hook
from spike_database.Coaches import Coaches
from spike_database.Competitions import Competitions
from spike_database.Contests import Contests
from spike_database.DiscordGuild import DiscordGuild
from spike_database.Leagues import Leagues
from spike_database.ResourcesRequest import ResourcesRequest
from spike_model.Competition import Competition
from spike_model.Contest import Contest
from spike_requester.CyanideApi import CyanideApi
from spike_utilities.Statistics import Statistics
from spike_utilities.Utilities import Utilities


def main():
    spike_logger.info("Update start")
    update()
    spike_logger.info("Update finished")
    sys.exit()


def update():
    excluded_server = Utilities.get_excluded_server()
    contest_db = Contests()
    rsrc = ResourcesRequest()
    league_db = Leagues()
    competition_db = Competitions()
    coach_db = Coaches()
    nb_request = 0
    checked_league = {}
    schedule_contest = {}
    played_contest = {}
    guilds_id = Utilities.get_guilds_id()

    for guild in guilds_id:
        try:
            if guild not in excluded_server:
                spike_logger.info(f"Update Contest for: {guild}")
                db = DiscordGuild(guild)
                common_db = ResourcesRequest()
                league_list = db.get_reported_leagues()

                for leagueId in league_list:
                    league_data = db.get_league_data(leagueId)
                    if league_data is not None:
                        platform_id = league_data[3]
                        league_name = league_data[2]
                        league_id = league_data[1]
                        league_logo = league_data[4]
                        platform = common_db.get_platform_label(platform_id)
                        spike_logger.info(f"League: {league_name} {platform}")
                        key = f"{league_name}-{platform}"
                        if checked_league.get(key) is None:
                            nb_request += 1
                            competitions = CyanideApi.get_competitions(league_name, platform=platform)
                            checked_league[key] = competitions
                        else:
                            competitions = checked_league.get(key)

                        for competition in competitions.get("competitions", []):
                            competition = Competition(competition)
                            competition_db_id = db.get_competition_id(competition.get_name(), platform_id)
                            if competition_db_id is not None:
                                competition_data = db.get_competition_data(competition_db_id)
                                comp_emoji = competition_data[5]
                                competition_db.add_or_update_raw_competition(competition, platform_id, comp_emoji)
                            else:
                                comp_emoji = rsrc.get_team_emoji(competition.get_league().get_logo())
                                competition_db.add_or_update_raw_competition(competition, platform_id)

                            competition_db.set_status(competition.get_id(), platform_id, competition.get_status())
                            if competition.get_format() != "ladder":
                                limit = int(competition.get_rounds_count()) * int(competition.get_team_max())
                                spike_logger.info(f"  - Competition: {competition.get_name()} {competition.get_id()}")

                                if competition.get_status() == 1:
                                    spike_logger.info("    - Save scheduled contest")
                                    key = f"{league_name}-{competition.get_name()}-{platform}"
                                    if schedule_contest.get(key) is None:
                                        nb_request += 1
                                        contests = CyanideApi.get_contests(league_name=league_name, competition_name=competition.get_name(),
                                                                           platform=platform, limit=limit)
                                        if contests is None or len(contests.get("upcoming_matches", [])) == 0:
                                            nb_request += 1
                                            contests = CyanideApi.get_contests(league_name=league_name, competition_name=competition.get_name(),
                                                                               platform=platform, limit=limit, exact=0)
                                        schedule_contest[key] = contests
                                    else:
                                        contests = schedule_contest.get(key)

                                    for match in contests.get("upcoming_matches", []):
                                        match = Contest(match)
                                        coach_db.add_or_update_coach(match.get_coach_home().get_id(), match.get_coach_home().get_name(), platform_id)
                                        coach_db.add_or_update_coach(match.get_coach_away().get_id(), match.get_coach_away().get_name(), platform_id)
                                        contest_db.add_or_update_contest(match, platform_id, competition.get_rounds_count(), comp_emoji)
                                        Statistics.get_contest_odds(match, platform_id, force=True)
                                else:
                                    spike_logger.info(f"    - Ignore schedule :: status: {competition.get_status()}")

                                contest_collected = competition_db.is_contest_fully_collected(competition.get_id(), platform_id)
                                if competition.get_status() > 0 and not contest_collected:
                                    spike_logger.info("    - Save played contest")
                                    key = "{}-{}-{}".format(league_name, competition.get_name(), platform)
                                    if played_contest.get(key) is None:
                                        nb_request += 1
                                        contests = CyanideApi.get_contests(league_name=league_name, competition_name=competition.get_name(),
                                                                           platform=platform, status="played", limit=limit)
                                        if contests is None or len(contests.get("upcoming_matches", [])) == 0:
                                            nb_request += 1
                                            contests = CyanideApi.get_contests(league_name=league_name, competition_name=competition.get_name(),
                                                                               platform=platform, status="played", limit=limit, exact=0)
                                        played_contest[key] = contests
                                    else:
                                        contests = played_contest.get(key)

                                    for match in contests.get("upcoming_matches", []):
                                        match = Contest(match)
                                        contest_db.add_or_update_contest(match, platform_id)
                                    if competition.get_status() == 2:
                                        spike_logger.info(f"    - Contest fully collected: {competition.get_name()} {competition.get_id()}")
                                        competition_db.set_contest_fully_collected(competition.get_id(), platform_id)
                                else:
                                    spike_logger.info(f"    - Ignore played :: status: {competition.get_status()} :: collected: {contest_collected}")
                            else:
                                spike_logger.info(f"  -Ignoring Ladder: {competition.get_name()} {competition.get_id()}")
                        league_db.add_or_update_league(league_name, league_id, platform_id, league_logo)
        except Exception as e:
            spike_logger.error(e)
    spike_logger.info(f"Nb request: {nb_request}")


sys.excepthook = except_hook

if __name__ == "__main__":
    main()
