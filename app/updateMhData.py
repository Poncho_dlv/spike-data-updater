#!/usr/bin/env python
# * coding: utf8 *


import sys

import discord

from __init__ import spike_logger, except_hook
from spike_database.MatchHistory import MatchHistory
from spike_database.Matches import Matches
from spike_database.LightMatches import LightMatches
from spike_settings.SpikeSettings import SpikeSettings
from spike_utilities.Utilities import Utilities


intents = discord.Intents.default()
client = discord.Client(intents=intents)

# Avoid restart the report in case of web socket restart
Already_running = False


def main():
    client.run(SpikeSettings.get_discord_token())


@client.event
async def on_ready():
    global Already_running
    if not Already_running:
        spike_logger.info("update_match_history_from_match_report connected")
        Already_running = True
        await update()
    else:
        spike_logger.warning("update_match_history_from_match_report reconnect")


async def update():
    spike_logger.info("Update match history")
    matches_db = Matches()
    matches_history_db = MatchHistory()
    matches = matches_history_db.get_match_to_update(2)
    match_list = []
    for match in matches:
        try:
            match_data = matches_db.get_match_data(match['_id'])
            if match_data is not None and match_data.get('match') is not None:
                match_list.append(match_data)
            else:
                spike_logger.error(f"Match: {match['_id']} invalid data")
                matches_history_db.set_full_data_not_found(match['_id'])
                matches_db.set_corrupted_data(match['_id'], True)
        except Exception as e:
            spike_logger.error(f"Error in get_match: {e}")
            pass

    Utilities.write_json_matches_into_match_history(match_list)
    spike_logger.info("Finished")
    sys.exit()


sys.excepthook = except_hook

if __name__ == "__main__":
    main()
