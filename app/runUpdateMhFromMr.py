#!/usr/bin/env python
# * coding: utf8 *


import sys

import discord

from __init__ import spike_logger, except_hook
from spike_database.Contests import Contests
from spike_database.Matches import Matches
from spike_model.Match import Match
from spike_requester.Utilities import Utilities as ReqUtils
from spike_settings.SpikeSettings import SpikeSettings
from spike_utilities.Statistics import Statistics
from spike_utilities.Utilities import Utilities

intents = discord.Intents.default()
client = discord.Client(intents=intents)

# Avoid restart the report in case of web socket restart
Already_running = False


def main():
    client.run(SpikeSettings.get_discord_token())


@client.event
async def on_ready():
    global Already_running
    if not Already_running:
        spike_logger.info("update_match_history_from_match_report connected")
        Already_running = True
        await update()
    else:
        spike_logger.warning("update_match_history_from_match_report reconnect")


async def update():
    spike_logger.info("Update match history")
    matches_db = Matches()
    contest_db = Contests()
    matches = matches_db.get_match_unsaved_in_match_history()
    nb_match = len(matches)
    spike_logger.info(f"Matches: {nb_match}")
    for match in matches:
        try:
            await process_message(contest_db, match)
        except AttributeError:
            spike_logger.error(f"invalid data {match['uuid']}")
            try:
                matches_db.remove_match(match["uuid"])
                ReqUtils.get_match(match["uuid"])
                await process_message(contest_db, match)
            except:
                spike_logger.error("Unable to fetch data")
                matches_db.remove_match(match["uuid"])
                continue

    coaches = Utilities.write_json_matches_into_match_history(matches)
    coaches = set(coaches)
    spike_logger.info("Update {} coaches".format(len(coaches)))

    for coach in coaches:
        try:
            spike_logger.info(f"coach {coach[0]} - {coach[1]}")
            process_coach(coach)
        except:
            spike_logger.error("Unable to process coach")

    spike_logger.info("Finished")
    sys.exit()


def process_coach(coach):
    stat_utils = Statistics()
    stat_utils.compute_and_save_coaches_win_rate(coach[0], coach[1])


async def process_message(contest_db, match):
    uuid = match["uuid"]
    match = Match(match)
    match.set_uuid(uuid)
    messages = contest_db.get_contest_messages(match.get_league_id(), match.get_competition_id(), match.get_coach_home().get_id(),
                                               match.get_coach_away().get_id(), match.get_platform_id())
    if messages is not None:
        for message in messages:
            channel_id = message["channel_id"]
            message_id = message["message_id"]
            channel = client.get_channel(channel_id)
            if channel is not None:
                try:
                    msg = await channel.fetch_message(message_id)
                    await msg.delete()
                except:
                    continue


sys.excepthook = except_hook

if __name__ == "__main__":
    main()
