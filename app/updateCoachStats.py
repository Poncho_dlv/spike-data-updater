#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys

from __init__ import spike_logger, except_hook
from spike_database.Coaches import Coaches
from spike_utilities.Statistics import Statistics


def main():
    update_coach_win_rate()
    sys.exit()


def update_coach_win_rate():
    coach_db = Coaches()
    coach_db.open_database()
    collection = coach_db.db["coaches"]
    coaches = collection.find({}, no_cursor_timeout=True)
    for coach in coaches:
        spike_logger.info(f"Coach: {coach['name']} - {coach['id']} - {coach['platform_id']}")
        Statistics.compute_and_save_coaches_win_rate(coach["id"], coach["platform_id"])
    coaches.close()
    coach_db.close_database()


sys.excepthook = except_hook

if __name__ == "__main__":
    main()
